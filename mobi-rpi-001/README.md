# How To

Um 

## CA

```bash
openssl genrsa   -out ca/ca.key   2048
openssl req   -new   -x509   -days 1826   -key ca/ca.key   -out ca/ca.crt   -subj "/CN=MQTT CA"
```

## Broker

```bash
openssl genrsa   -out mosquitto-data/certs/broker/broker.key   2048
openssl req -new -key mosquitto-data/certs/broker/broker.key -out mosquitto-data/certs/broker/broker.csr -config broker.conf
openssl x509   -req   -in mosquitto-data/certs/broker/broker.csr   -CA ca/ca.crt   -CAkey ca/ca.key   -CAcreateserial   -out mosquitto-data/certs/broker/broker.crt   -days 360 -extensions v3_req -extfile mosquitto-data/certs/broker/broker_ext.conf
cp mosquitto-data/certs/broker/broker.crt zigbee2mqtt-data/ca.crt
```
