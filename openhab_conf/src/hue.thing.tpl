
Thing mqtt:topic:%%DEV%% "%%DEV%%" (mqtt:broker:%%BROKER%%) {
    Channels:
    Type switch : state "Status" [ 
        stateTopic="zigbee2mqtt/%%DEV%%/state",
        commandTopic="zigbee2mqtt/%%DEV%%/set/state",
        on="ON",
        off="OFF"
    ]
    Type dimmer : brightness "Helligkeit" [
        stateTopic="zigbee2mqtt/%%DEV%%/brightness",
        commandTopic="zigbee2mqtt/%%DEV%%/set/brightness",
        min=0,
        max=255,
        step=1 
    ]
    Type dimmer : temperature "Wärme" [
        stateTopic="zigbee2mqtt/%%DEV%%/color_temp",
        commandTopic="zigbee2mqtt/%%DEV%%/set/color_temp",
        min=153,
        max=500,
        step=1 
    ]
    Type color : color "Farbe" [ 
        stateTopic="zigbee2mqtt/%%DEV%%",
        commandTopic="zigbee2mqtt/%%DEV%%/set",
        colorMode="XYY",
        transformationPatternOut="JS:brightnessOut.js",
        transformationPattern="JS:brightnessIn.js"
    ]
}