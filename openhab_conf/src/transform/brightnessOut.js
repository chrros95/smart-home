(function (x) {
    var tmp = x.split(',');
    tmp[2] = tmp[2] * 2.54;
    return '{"color":{"x":"' + tmp[0] + '","y":"' + tmp[1] + '"},"brightness":"' + tmp[2] + '"}'
})(input)