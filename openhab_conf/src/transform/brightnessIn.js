(function (i) {
    var logger = Java.type('org.slf4j.LoggerFactory').getLogger('org.openhab.transform.brightnessIn');
    var colorFromMqtt = JSON.parse(i);
    if (colorFromMqtt.color === undefined) {
        colorFromMqtt.color = {};
    }
    var result = ((colorFromMqtt.color.h || 0) /100)+ "," + ((colorFromMqtt.color.s || 0)/100) + "," + ((colorFromMqtt.brightness || 0) / 254 * 100);
    return result;
})(input)