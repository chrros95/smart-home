
Group  e_%%DEV%%             "%%NAME%%"                                (%%GRP%%)              ["Sensor"]                {alexa="TemperatureSensor"}
Number e_%%DEV%%_voltage     "%%NAME%% Spannung"         <energy>      (%%EGRP%%_voltage)                               {channel="mqtt:topic:%%DEV%%:voltage"}
Number e_%%DEV%%_temperature "%%NAME%% Temperatur"       <temperature> (%%EGRP%%_temperature) ["Temperature"]           {channel="mqtt:topic:%%DEV%%:temperature", alexa="CurrentTemperature" [name="%%NAME%%"]}
Number e_%%DEV%%_humidity    "%%NAME%% Luftfeuchtigkeit" <humidity>    (%%EGRP%%_humidity)    ["Humidity"]              {channel="mqtt:topic:%%DEV%%:humidity", alexa="CurrentHumidity" [name="%%NAME%%"]}
Number e_%%DEV%%_battery     "%%NAME%% Batteriestatus"   <battery>     (%%EGRP%%_battery)     ["Measurement", "Energy"] {channel="mqtt:topic:%%DEV%%:battery", alexa="BatteryLevel" [name="%%NAME%%"]}
