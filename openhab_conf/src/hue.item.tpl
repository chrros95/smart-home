
Group     e_%%DEV%%            "%%NAME%%"                          (%%GRP%%)             ["Lightbulb"]                    {alexa="Light"}
Switch    e_%%DEV%%_light      "%%NAME%% Ein/Aus"    <switch>      (%%EGRP%%_light)      ["Switch"]                       {channel="mqtt:topic:%%DEV%%:state", alexa="PowerState" [name="%%NAME%%"]}
Dimmer    e_%%DEV%%_dimmer     "%%NAME%% Helligkeit" <light>       (%%EGRP%%_dimmer)     ["Light"]                        {channel="mqtt:topic:%%DEV%%:brightness", alexa="Brightness" [name="%%NAME%%"]}
Color     e_%%DEV%%_color      "%%NAME%% Farbe"      <colorpicker> (%%EGRP%%_color)      ["Light"]                        {channel="mqtt:topic:%%DEV%%:color", alexa="Color",ga="Light" [name="%%NAME%%"]}
Dimmer    e_%%DEV%%_color_temp "%%NAME%% Wärme"                    (%%EGRP%%_color_temp) ["Light", "ColorTemperature"]    {channel="mqtt:topic:%%DEV%%:temperature", alexa="ColorTemperature" [name="%%NAME%%"]}
