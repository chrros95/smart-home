
Thing mqtt:topic:%%DEV%% "%%DEV%%" (mqtt:broker:%%BROKER%%) {
    Channels:
    Type number: signal "Signalstärke" [ stateTopic="zigbee2mqtt/%%DEV%%/linkquality" ]
    Type switch: battery "Batteriestatus" [
        stateTopic="zigbee2mqtt/%%DEV%%/battery_low",
        on="true",
        off="false"
    ]
    Type number: temperature "Temperatur" [ stateTopic="zigbee2mqtt/%%DEV%%/local_temperature" ]

    Type number: target_temperature "Soll Temperatur" [
        stateTopic="zigbee2mqtt/%%DEV%%/current_heating_setpoint",
        commandTopic="zigbee2mqtt/%%DEV%%/set/current_heating_setpoint",
        min=5,
        max=35,
        step=0.5
    ]
    Type number: local_temperature_calibration "Temperaturkorrektur " [
        stateTopic="zigbee2mqtt/%%DEV%%/local_temperature_calibration",
        commandTopic="zigbee2mqtt/%%DEV%%/set/local_temperature_calibration",
        min=-9,
        max=9,
        step=0.5
    ]
    Type switch: force "Force" [
        stateTopic="zigbee2mqtt/%%DEV%%/force",
        commandTopic="zigbee2mqtt/%%DEV%%/set/force",
        on="open",
        off="normal"
    ]
    Type switch: lock "Kindersicherung" [
        stateTopic="zigbee2mqtt/%%DEV%%/child_lock",
        commandTopic="zigbee2mqtt/%%DEV%%/set/state",
        on="LOCK",
        off="UNLOCK"
    ]
    Type switch: window_state "Fensterstatus" [ 
        stateTopic="zigbee2mqtt/%%DEV%%/window_open",
        on="true",
        off="false"
    ]
}
