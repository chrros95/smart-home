
Group  e_%%DEV%%                    "%%NAME%%"                                                          (%%GRP%%)                     ["RadiatorControl"]         {alexa="Thermostat"}
Number e_%%DEV%%_signal             "%%NAME%% Signal"                                     <signal>      (%%EGRP%%_signal)             ["QualityOfService"]        {channel="mqtt:topic:%%DEV%%:signal"}
Switch e_%%DEV%%_battery            "%%NAME%% Batteriestatus"                             <battery>     (%%EGRP%%_battery)            ["LowBattery"]              {channel="mqtt:topic:%%DEV%%:battery", alexa="BatteryLevel" [name="%%NAME%%"]}
Switch e_%%DEV%%_force              "%%NAME%% Force"                                                    (%%EGRP%%_force)                                          {channel="mqtt:topic:%%DEV%%:force"}
Switch e_%%DEV%%_lock               "%%NAME%% Kindersicherung"                                          (%%EGRP%%_lock)                                           {channel="mqtt:topic:%%DEV%%:lock"}
Switch e_%%DEV%%_dummy              "%%NAME%% Dummy Status"                                             (%%EGRP%%_dummy)              ["Control"]                 {alexa="ThermostatFan" [name="%%NAME%%"]}
Number:Temperature e_%%DEV%%_temperature        "%%NAME%% Temperatur [%.2f °C]"           <temperature> (%%EGRP%%_temperature)        ["Temperature"]             {channel="mqtt:topic:%%DEV%%:temperature", alexa="CurrentTemperature" [name="%%NAME%%"]}
Number:Temperature e_%%DEV%%_target_temperature "%%NAME%% Soll-Temperatur [%.2f °C]"      <temperature> (%%EGRP%%_target_temperature) ["Control", "Temperature"]  {channel="mqtt:topic:%%DEV%%:target_temperature", alexa="TargetTemperature" [name="%%NAME%%"]}
Number:Temperature e_%%DEV%%_local_temperature_calibration "%%NAME%% Temperaturkorrektur"               (%%EGRP%%_target_temperature) ["Control"]                 {channel="mqtt:topic:%%DEV%%:local_temperature_calibration"}
