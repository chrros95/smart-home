
Group:Switch:OR(ON,OFF)  %%GRP%%_light      "%%NAME%% Ein/Aus [(%d)]" <switch>      (%%GRP%%)    ["Light"]             {alexa="Light" [name="%%NAME%% Lichter"]}
Group:Dimmer             %%GRP%%_dimmer     "%%NAME%% Helligkeit"     <light>       (%%GRP%%)    ["Light"]             {alexa="Brightness" [name="%%NAME%% Lichter"]}
Group:Color              %%GRP%%_color      "%%NAME%% Farbe"          <colorpicker> (%%GRP%%)    ["Light"]             {alexa="Color" [name="%%NAME%% Lichter"],ga="Light"}
Group:Dimmer             %%GRP%%_color_temp "%%NAME%% Wärme"                        (%%GRP%%)    ["ColorTemperature"]  {alexa="ColorTemperature" [name="%%NAME%% Lichter"]}
