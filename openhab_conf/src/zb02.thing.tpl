
Thing mqtt:topic:%%DEV%% "%%DEV%%" (mqtt:broker:%%BROKER%%) {
    Channels:
    Type number : voltage "Spannung" [  stateTopic="zigbee2mqtt/%%DEV%%/voltage" ]
    Type number : temperature "Temperatur" [  stateTopic="zigbee2mqtt/%%DEV%%/temperature" ]
    Type number : humidity "Luftfeuchtigkeit" [  stateTopic="zigbee2mqtt/%%DEV%%/humidity" ]
    Type number : battery "Batteriestatus" [  stateTopic="zigbee2mqtt/%%DEV%%/battery" ]
}