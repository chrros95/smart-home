const { rules, triggers, actions } = require('openhab');

const limit = 65

rules.JSRule({
    name: "Alert High Humidity",
    description: "A Light will be flushed when the humidity is above the recommended value",
    triggers: [
        triggers.GroupStateChangeTrigger('g_livingroom_humidity'),
        triggers.GroupStateChangeTrigger('g_bedroom_humidity'),
        triggers.GroupStateChangeTrigger('g_kitchen_humidity'),
        triggers.GroupStateChangeTrigger('g_office_humidity')
    ],
    execute: (event) => {
        console.log('Notification?',event.newState, event.oldState, event.newState >=limit , event.oldState < limit ,event.newState > event.oldState)
        if (event.newState >=limit && event.oldState < limit && event.newState > event.oldState) {
            console.log('Notification triggered',event, event.newState, event.oldState)
            actions.NotificationAction.sendBroadcastNotification('Die Lufttemperatur von ' + event.itemName + ' ist ' + event.newState);
        }
    },
    tags: ["Balcony", "Lights"],
    id: "a_livingroom_humidity"
});