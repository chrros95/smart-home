R_PATH="/var/data/openhab/config"
PERL5LIB="./local/lib/perl5:$PERL5LIB" perl build.pl
scp -r build/* chrros95@192.168.177.10:${R_PATH}
ssh chrros95@192.168.177.10 "sudo -S chown -R 9001:9001 ${R_PATH} && sudo -S setfacl -R -m u:chrros95:rwx ${R_PATH}"
kubectl  rollout restart -n openhab deployment
kubectl get pods -n openhab -w