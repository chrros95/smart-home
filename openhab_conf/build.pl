use strict;
use warnings;
use utf8;
use File::Path            qw(make_path remove_tree);
use File::Copy::Recursive qw(fcopy dircopy);
use Utils::Files;

my $SRC       = './src';
my $STATIC    = $SRC . q(/static);
my $OUT       = './build';
my $templates = {};
my $devices   = {};
my $groups    = {};
my $vars      = {};
my $FH;

$devices = Utils::Files::read_file(
    './devices.csv',
    sub {
        my ($line) = @_;
        my ( $type, $device, $broker, $name, $group ) = split m/;/smx, $line;
        $devices->{$device} = {
            type   => $type,
            device => $device,
            broker => $broker,
            name   => $name
        };
        $templates->{$type} = {};
        if ($group) {
            for my $g ( split m/,/smx, $group ) {
                if ( !defined $groups->{$g} ) {
                    $groups->{$g} = { devices => {} };
                }
                if ( !defined $devices->{$device}->{primary_group} ) {
                    $devices->{$device}->{primary_group} = $g;
                }
                push @{ $groups->{$g}->{devices}->{$type} }, $device;
                push @{ $devices->{$device}->{groups} },     $g;
            }
        }
        return $devices;
    }
);

$groups = Utils::Files::read_file(
    './groups.csv',
    sub {
        my ($line) = @_;
        my ( $group, $name ) = split m/;/smx, $line;
        if ( defined $groups->{$group} ) {
            $groups->{$group}->{group} = $group;
            $groups->{$group}->{name}  = $name;
        }
        return $groups;
    }
);

$vars = Utils::Files::read_file(
    './.env',
    sub {
        my ($line) = @_;
        my ( $var, $value ) = $line =~ m/([^=]*)=(.*)/smx;
        $vars->{$var} = $value;
        return $vars;
    }
);

remove_tree($OUT);
make_path($OUT);

for my $d (qw(transform sitemaps things items services persistence automation))
{
    dircopy( $SRC . q(/) . $d, $OUT . q(/) . $d );
}

my $file_name;
for my $type ( keys %{$templates} ) {
    for my $key (qw(thing item group)) {
        $file_name = qq($SRC/$type.$key.tpl);
        if ( -e $file_name ) {
            $templates->{$type}->{$key} = Utils::Files::read_file($file_name);
            $templates->{$type}->{$key} =
              [ split m/\n/smx, $templates->{$type}->{$key} ];
        }
    }
}

my ( $tpl, $out, $name, $pattern, $suffix, $channels );
for my $device ( values %{$devices} ) {
    $tpl = $templates->{ $device->{type} };
    if ( !defined $tpl ) {
        print 'Template for tye "'
          . $device->{type}
          . '" ist nicht definiert.' . "\n";
        next;

    }
    if ( $tpl->{thing} ) {
        $out  = join "\n", @{ $tpl->{thing} };
        $out  = $out =~ s/%%DEV%%/$device->{device}/rgsmx;
        $out  = $out =~ s/%%BROKER%%/$device->{broker}/rgsmx;
        $name = $device->{device} . q(.things);
        if ( $device->{primary_group} ) {
            $name = $device->{primary_group} . q(_) . $name;
        }
        Utils::Files::save( $OUT . q(/things/) . $name, $out );
    }
    if ( $tpl->{item} ) {
        my @out_lines;
        for my $line ( @{ $tpl->{item} } ) {
            ( $pattern, $suffix ) = $line =~ m/(%%EGRP%%_([\w_]*))/smx;
            $out = $line;
            if ( $pattern && $device->{primary_group} ) {
                $channels =
                  q(e_) . $device->{device} . q(,) . join qq(_$suffix,),
                  @{ $device->{groups} };
                $channels = $channels . qq(_$suffix);
                $out      = $out =~ s/%%EGRP%%_$suffix/$channels/rgsmx;
            }
            push @out_lines, $out;
        }
        $out  = join "\n", @out_lines;
        $out  = $out =~ s/%%DEV%%/$device->{device}/rgsmx;
        $out  = $out =~ s/%%NAME%%/$device->{name}/rgsmx;
        $out  = $out =~ s/%%GRP%%/$device->{primary_group}/rgsmx;
        $name = $device->{device} . q(.items);
        if ( $device->{primary_group} ) {
            $name = $device->{primary_group} . q(_) . $name;
        }
        Utils::Files::save( $OUT . q(/items/) . $name, $out );
    }
}

for my $group ( values %{$groups} ) {
    my $file_name = $OUT . q(/items/) . $group->{group} . q(.items);
    my @group_lines;
    for my $type ( keys %{ $group->{devices} } ) {
        if ( !defined $templates->{$type}->{group} ) {
            next;
        }
        for my $line ( @{ $templates->{$type}->{group} } ) {
            ( $pattern, $suffix ) = $line =~ m/(%%CHANNEL%%:(\w*))/smx;
            $out = $line;
            if ($pattern) {
                $channels = join qq(:$suffix",channel="mqtt:topic:),
                  @{ $group->{devices}->{$type} };
                $channels = qq(channel="mqtt:topic:$channels:$suffix");
                $out      = $out =~ s/%%CHANNEL%%:$suffix/$channels/rgsmx;
                $out      = $out =~ s/%%NAME%%/$group->{name}/rgsmx;
            }
            push @group_lines, $out;
        }
    }

    $out = join "\n", sort @group_lines;
    $out = $out =~ s/%%NAME%%/$group->{name}/rgsmx;
    $out = $out =~ s/%%GRP%%/$group->{group}/rgsmx;
    Utils::Files::save( $file_name, $out );
}

my @files = Utils::Files::find( $OUT,
    $Utils::Files::RECURSIVE | $Utils::Files::ONLY_FILES );
for my $file (@files) {
    if ( $file !~ m/\.cfg$/smx && $file !~ m/\.things$/smx ) {
        next;
    }
    for my $key ( keys %{$vars} ) {
        Utils::Files::replace( $file, q(%%) . $key . q(%%), $vars->{$key} );
    }
}
